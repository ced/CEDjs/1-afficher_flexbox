Traduction de l'application [CEDy](https://framagit.org/ced/cedy/1-afficher) (Calculette : Eco-déplacements, écrite en Python)
dans le langage javascript...

Cette version utilise les feuilles de styles pour optimiser la présentation.

Voir la [demo](https://ced.frama.io/CEDjs/1-afficher_flexbox/) mise
en œuvre via les pages de framagit et l'intégration continue...
